<?php

require "vendor/autoload.php";

use Amp\Loop;
use Amp\File;

Loop::run(function() {
    $foo = yield File\get("foo");
    print $foo;
});
